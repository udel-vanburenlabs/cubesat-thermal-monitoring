#!/bin/env python3

import sys
import os

import state

version = '0.1.0'

commands_dict = {
    'noop': {'hex': 0x00},
    'get_data_frame': {'hex': 0x01},
    'get_n_frames': {'hex': 0x02},
    'has_data_frames': {'hex': 0x03},
    'start_stepper_1': {'hex': 0x04},
    'start_stepper_2': {'hex': 0x05},
    'stop_stepper_1': {'hex': 0x06},
    'stop_stepper_2': {'hex': 0x07},
    'reset_mcu': {'hex': 0x08},
    'clear_buffer': {'hex': 0x09},
    'set_laser_1': {'hex': 0x0A},
    'set_laser_2': {'hex': 0x0B}

}

data_frame_dict = {

}

errors_dict = {

}


def opts():
    print()
    print('(1) See commands list')
    print('(2) Test connection with MCU')
    print('(3) Run commands on MCU')
    print('(4) Exit')


def user_inp():
    inp_line = input('> ')
    entry = int(inp_line)
    if entry == 4:
        state.running = False
    elif entry == 3:
        pass
    elif entry == 2:
        pass
    elif entry == 1:
        pass
    else:
        opts()


def start(argv: list):
    print('CubeLab Pico Command Test Suite {0}'.format(version))
    print('=====================================')
    opts()
    while state.running:
        user_inp()


if __name__ == '__main__':
    if 'nt' not in os.name:
        start(sys.argv)
    else:
        print('FATAL: can only communicate with RPi Pico on POSIX systems', file=sys.stderr)
        exit(-1)
