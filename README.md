# CubeSat Microcontroller Code

Systems level code for our Raspberry Pi Pico controlled temperature monitoring, stepper driver,
and sensor data collection board.

The `earlphilhower` core with a GCC cross-compiler to ARM32 is used for this project.
Small test files use the traditional Arduino ".ino" format instead, and can be run
directly from the Arduino IDE.

The `src/arduino/thermal-power-stress-test` project contains the code necessary to test
the MCU system under full load with lasers and steppers on (in one flow facility). This code
can be uploaded to the MCU for the initial NanoRacks tests. This is another PlatformIO project that
must be opened in VSCode. It does not have any additional dependencies besides the Arduino core libraries.

***

## Installation

#### From project directory:
 - Open the project in a VSCode editor with the PlatformIO extension installed.
 - Install the `libboost-dev`, and `libgtest-dev` dependencies.
 - Optionally install the Arduino IDE with the Raspberry Pi Pico core to run `.ino` PoC files.
 - Run the "Upload" task on the PlatformIO extension menu for the "pico" project.

## Usage
 - For full functionality, the Pico must be soldered to the CubeLab microcontroller board (latest)
    version is Rev10).
 - Completed PCB should be plugged in via micro USB cable to a USB 2.0 port on the OrangePi SBC.
 - SBC must be running the Postgres DB, Rest API application, and Picotunnel applications already.


## Authors 
Galen Nare