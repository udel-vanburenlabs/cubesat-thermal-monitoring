copy = False

def isvalid(line):
    return line.strip().startswith('src/arduino/cubelab-pico-code/') or line.strip().startswith('In file included from src/arduino/cubelab-pico-code/')

with open('buildwarn.log', 'r') as log:
    with open('parsedlog.log', 'w+') as outfile:
        for line in log:
            if isvalid(line):
                copy = True
            if copy:
                if not isvalid(line) and not line.startswith(' '):
                    copy = False
                else:
                    outfile.write(line)
                    print(line, end='')
