#include "MAX51X.h"
#include <Wire.h>

MAX518 dvc518(0x2C);
int level = 0;

void setup() {
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();
  Serial.begin(9600);
  dvc518.begin();
}

void loop() {
  delay(50);
  dvc518.setBothDac((uint8_t) level % 256, (uint8_t) level % 256);
  level += 1;
  Serial.println(level % 256);
}
