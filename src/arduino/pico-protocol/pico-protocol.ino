#include <string>

#define PICO_ID 0

uint64_t timeOffsetMicroSec = 0;

void executeCommand(std::string& command) {
    if (command.substr(0, 8) == "#PICO_ID") {
        Serial.print("#PICO_ID ");
        Serial.println(PICO_ID);
    } else if (command.substr(0, 12) == "#TIME_OFFSET") {
        timeOffsetMicroSec = std::stoull(command.substr(12, command.size() - 12));
        Serial.println("#TIME_ACK");
    } else {
        Serial.println(command.c_str());
    }
}

void getIncomingCommand() {
    std::string command = Serial.readStringUntil('\n').c_str();
    if (command[0] == '#') {
        executeCommand(command);
    }
}

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
    
    Serial.begin(115200);
}

void loop() {
    if(Serial.available()) {
        getIncomingCommand();
    }

    Serial.println("TEST");
    
    delay(100);
}
