#include <Stepper.h>

#define STEPS 200 // 200 steps/rev (1.8deg per step)

Stepper stepper(STEPS, 14, 15, 16, 17);
Stepper stepper2(STEPS, 6, 7, 8, 9);

int c = 300;
int maxs = 800;

void setup() {
  Serial.begin(9600);

  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);

  pinMode(19, OUTPUT);

  digitalWrite(19, HIGH);
  
  stepper.setSpeed(c); // fasterer
}

void loop() {
  if (c < maxs) {
    c += 2;
    stepper.setSpeed(c);
    Serial.println(c);
  }

  stepper.step(1);
  stepper2.step(1);
}
