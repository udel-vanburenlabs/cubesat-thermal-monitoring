/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#ifndef CUBELAB_CONTROLLER_H
#define CUBELAB_CONTROLLER_H

// Hardware includes
#include <Arduino.h>
#include <Wire.h>
#include <bmp3_defs.h>
#include <Adafruit_BMP3XX.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL375.h>
#include <pico/stdlib.h>
#include <pico/multicore.h>
#include <hardware/uart.h>
#include <hardware/gpio.h>
#include <PacketSerial.h>

// Local source includes
#include "HallEncoder.h"
#include "commandParser.h"
#include "picoCommands.h"

/**
 * Set up hardware I2C on pico
 */
void initializeI2C();

/**
 * Set up gyro/accelerometer for I2C. Takes a pointer to an IMU object.
 */
void initializeIMU(Adafruit_ADXL375* imu);

/**
 * Reads the data from the IMU object using a pointer to it. Outputs a DataFrame struct.
 */
DataFrame getDataFrame(Adafruit_ADXL375* imu);

// Function prototypes for BMP388 temperature sensors
/**
 * Init BMP388 temp and pressure sensors for I2C. Takes a 2-element list of sensor objects.
 */
void initializeBMP388Sensors(Adafruit_BMP3XX bmpSensors[]);

/**
 * Reads the data from the given sensor and outputs a data struct.
 */
BMP388Data readBMP388Data(Adafruit_BMP3XX bmpSensor);

// Function prototypes for SN754410NE quad-half-H bridge drivers and stepper motors
/**
 * Sets up the flow-facility stepper motors. Takes a 2-element list.
 */
void initializeStepperMotors(Stepper motors[]);

///**
// * Sets the speed of the stepper motor using a pointer to it.
// */
//void setStepperMotorSpeed(Stepper* motor, unsigned int speed);
//void stepStepperMotor(Stepper* motor, int steps);

// Function prototypes for MOSFETs and PWM control for lasers
/**
 * Sets up the MAX518 driver for pico laser array for I2C. Takes a pointer to the MAX518 object.
 */
void initializeLasers(MAX518* max518);

/**
 * Polls the SBC over serial for the next commands. Automatically executes them if any are recieved.
 */
void receiveCommandsFromComputer();

/**
 * Executes the provided command. Excess arguments are ignored.
 */
void executeCommand(CommandStruct& commandStruct);

/**
 * Parses and executes a list of string commands from a SBC, in order.
 */
void executeCommands(std::vector<std::string> commands);

/**
 * Blink error code function for debugging.
 */
void blinkError(int blinksPerSec);

#endif // CUBELAB_CONTROLLER_H
