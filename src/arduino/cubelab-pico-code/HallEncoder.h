/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <Arduino.h>

#include "util.h"

#define ENCODER0 26
#define ENCODER1 27 // TODO
#define ENCODER0_PERIODS_MAXLEN 50
#define ENCODER1_PERIODS_MAXLEN 50
#define ENC_PULSES_PER_REV 4.0
#define ENC_LOOP_DELAY 10
#define ENC_10S_AVG_LEN (10000 / ENC_LOOP_DELAY)

/**
 * Driver class for the 3144 hall effect sensor.
 * Used to emulate a magnetic encoder.
 */
class HallEncoder {
    public:
		/**
		 * Sets up the encoder with the given digital pin.
		 * Note that this registers an interrupt on the given pin.
		 */
        HallEncoder(pin_size_t pin);

        /**
         * Deregisters attached interrupts and destroys the object.
         */
        ~HallEncoder();

        /**
         * Gets the time since the last encoder pulse, in microseconds.
         */
        unsigned long getCurrentEncoderTime();

        /**
         * Gets the total time between encoder pulses in the last cycle, in microseconds.
         */
        unsigned long getLastEncoderTime();

        /**
         * Reads the encoder, processes the data, and returns the current rolling 10s average as a double.
        */
        double read();

        /**
         * Per-encoder ISR function
         */
        static void encoderISR(HallEncoder* encoder);

        /**
         * ISR function for all registered encoders.
         */
        static void encoderISR(void* param);
        
    private:
        pin_size_t pin;
        volatile unsigned long encoderTime = 0;
        unsigned long encoderLastTime = 0;

        unsigned long encoderPeriods[ENCODER0_PERIODS_MAXLEN];
        unsigned int periodsI = 0;

        double encoder10sAvgArr[ENC_10S_AVG_LEN];
        unsigned int enc10sAvgI = 0;
};
