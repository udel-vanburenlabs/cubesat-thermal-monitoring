/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#ifndef _CUBELAB_CONSTANTS_H
#define _CUBELAB_CONSTANTS_H

#define PICO_ID 0

#define I2C0_SDA 12
#define I2C0_SCL 13

#define I2C1_SDA 10
#define I2C1_SCL 11

#define LASER_ADDR 0x2C
#define BMP388_ADDR0 0x76
#define BMP388_ADDR1 0x77
#define IMU_ADDR0 0x53

#define DEBUG0 5

#define STEPPERS_EN 20

#define LED 25

#define UNUSED_ANALOG_PIN 28

#define STEPPER1_1A 14
#define STEPPER1_2A 15
#define STEPPER1_3A 16
#define STEPPER1_4A 17

#define STEPPER2_1A 6
#define STEPPER2_2A 7
#define STEPPER2_3A 8
#define STEPPER2_4A 9

#define MAX_STEPPER_SPEED 600
#define MIN_STEPPER_SPEED 200
#define MIN_STEPPER_RAMP 50

#define STEPS_PER_REV 200

#define BAUD_RATE 115200

#endif
