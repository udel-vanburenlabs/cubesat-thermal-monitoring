/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "picoCommands.h"

extern uint64_t systemMicroSecOffset;

std::string encodeDataFrame() {
  UARTDataPacket packet;

  packet.picoId = PICO_ID;

  uint64_t maxTimestamp = 0;

  if (bmpData0.size() > 0) {
    packet.has_bmpData0 = true;
    packet.bmpData0 = bmpData0.front();
    maxTimestamp = std::max(maxTimestamp, packet.bmpData0.timestamp);
  } else {
    packet.has_bmpData0 = false;
  }
  
  if (bmpData1.size() > 0) {
    packet.has_bmpData1 = true;
    packet.bmpData1 = bmpData1.front();
    maxTimestamp = std::max(maxTimestamp, packet.bmpData1.timestamp);
  } else {
    packet.has_bmpData1 = false;
  }

  if (dataFrames.size() > 0) {
    packet.has_sensorData = true;
    packet.sensorData = dataFrames.front();
    maxTimestamp = std::max(maxTimestamp, packet.sensorData.timestamp);
  } else {
    packet.has_sensorData = false;
  }

  if (maxTimestamp == 0) {
    packet.timestamp = getAdjustedMicroSecTime();
  } else {
    packet.timestamp = maxTimestamp;
  }

  StaticJsonDocument<8192> doc;

  doc["picoId"] = packet.picoId;
  doc["timestamp"] = packet.timestamp;
  doc["picoTemp"] = packet.sensorData.picoTemperature;

  if (packet.has_bmpData0) {
    doc["sensTemp0"] = packet.bmpData0.temperature;
    doc["sensPress0"] = packet.bmpData0.pressure;
  }

  if (packet.has_bmpData1) {
    doc["sensTemp1"] = packet.bmpData1.temperature;
    doc["sensPress1"] = packet.bmpData1.pressure;
  }

  if (packet.has_sensorData) {
    if (PICO_ID) {
      doc["sensAccelX1"] = packet.sensorData.accel_x;
      doc["sensAccelY1"] = packet.sensorData.accel_y;
      doc["sensAccelZ1"] = packet.sensorData.accel_z;
      doc["sensGyroX1"] = packet.sensorData.gyro_x;
      doc["sensGyroY1"] = packet.sensorData.gyro_y;
      doc["sensGyroZ1"] = packet.sensorData.gyro_z;
    } else {
      doc["sensAccelX0"] = packet.sensorData.accel_x;
      doc["sensAccelY0"] = packet.sensorData.accel_y;
      doc["sensAccelZ0"] = packet.sensorData.accel_z;
      doc["sensGyroX0"] = packet.sensorData.gyro_x;
      doc["sensGyroY0"] = packet.sensorData.gyro_y;
      doc["sensGyroZ0"] = packet.sensorData.gyro_z;
    }
  }

  // TODO implement rpm data from hall effect encoders

  char buffer[8192];

  serializeJson(doc, buffer);

  return buffer;
}

// TODO handle ACK and resend requests
// TODO make data send non-blocking

void handleStartStepper1(uint32_t stepperSpeed) {
  steppers[0].setSpeed(stepperSpeed);
}

void handleStartStepper2(uint32_t stepperSpeed) {
  steppers[1].setSpeed(stepperSpeed);
}

void handleStopStepper1() {
  steppers[0].setSpeed(0);
}

void handleStopStepper2() {
  steppers[1].setSpeed(0);
}

void handleResetMCU() {
  watchdog_reboot(0, 0, 500);
}

void handleClearDataBuffer() {
  bmpData0.clear();
  bmpData1.clear();
  dataFrames.clear();
}

void handleSetLaserState(unsigned int flowFacility, uint32_t brightness) {
  if (flowFacility == 0) {
    max518.setBothDac(brightness, 0);
  } else {
    max518.setBothDac(0, brightness); 
  }
}

bool didHostACK() {
  return false; // TODO
}

void executeCommand(std::string& command) {
    if (command.substr(0, 8) == "#PICO_ID") {
        Serial.print("#PICO_ID ");
        Serial.println(PICO_ID);
    } else if (command.substr(0, 12) == "#TIME_OFFSET") {
    	systemMicroSecOffset = std::stoull(command.substr(12, command.size() - 12));
    	Serial.println("#TIME_ACK");
    	Serial.print("Time offset set to ");
    	Serial.print(systemMicroSecOffset);
		  Serial.println();
    } else {
        Serial.println(command.c_str());
    }
}

void getIncomingCommand() {
    std::string command = Serial.readStringUntil('\n').c_str();
    if (command[0] == '#') {
        executeCommand(command);
    }
}
