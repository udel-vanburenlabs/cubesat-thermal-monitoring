/*
 MIT License

 Copyright (c) 2023 Galen Nare

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

#include "cubelabCommon.h"

// MAIN FILE

/**
 * System time offset in microseconds. 
 * We have this because the Pico does not have a real RTC.
 * This is set when the Pico calls the API for the time from the SBC.
*/
uint64_t systemMicroSecOffset = 0;

// Data holders for sending to SBC
std::list<DataFrame> dataFrames = std::list<DataFrame>();
std::list<BMP388Data> bmpData0 = std::list<BMP388Data>();
std::list<BMP388Data> bmpData1 = std::list<BMP388Data>();

// Data holders for case info
int currentCaseId = 0;
std::map<CaseParams, CubelabCase> casesMap =
		std::map<CaseParams, CubelabCase>();
std::vector<CaseLogMessage> caseMessages = std::vector<CaseLogMessage>();

// Temp/pressure sensors
Adafruit_BMP3XX bmpSensors[2];

HallEncoder encoders[2] = {HallEncoder(ENCODER0), HallEncoder(ENCODER1)};


// gyro/accel sensor
Adafruit_ADXL375 imu = Adafruit_ADXL375(0x1337);

// Laser driver
MAX518 max518(LASER_ADDR);

// Steppers
Stepper steppers[2];

/**
 * Gets the CPU program counter value for debugging. 
 * Unused in production.
 * Must be inlined to work at all.
 */
__attribute__((always_inline)) inline uint32_t getCurrentPC() {
    uint32_t pc;
    asm volatile ("mov %0, pc" : "=r" (pc));
    return pc;
}

/**
 * Restart the MCU if the program dies for any reason (as in Segfault).
 * Ideally this never gets called.
 */
extern "C" void HardFault_Handler(void) {
	rp2040.restart();
}

/**
 * Core 0 setup: sensors and communication
 */
void setup() {
	rp2040.f_cpu(); // Verify pico SDK is working

	// Flash LED to signal reset
	pinMode(LED, OUTPUT);
	digitalWrite(LED, HIGH);
	delay(100);

	Serial.begin();

	// Wait for serial to actually be connected
	while(!Serial || !Serial.availableForWrite()) {
		delay(100);
	}

	// Wait for system config commands
    // Calls the API for the correct system time offset
#ifndef NO_PICO_CONFIG
	while(systemMicroSecOffset == 0) {
		getIncomingCommand();
		delay(10);
	}
#endif

	// Initialize sensors
	initializeI2C();

	pinMode(DEBUG0, INPUT_PULLUP);

	initializeLasers(&max518);

	initializeIMU(&imu);
	initializeBMP388Sensors(bmpSensors);

	// Resets the CPU if the program freezes
	watchdog_enable(5000, 1); // Enable 5sec watchdog timer

	// Turn off the LED to indicate init is complete
	digitalWrite(LED, LOW);

	// This should be a sufficiently random seed
	// Uses microsec time with offset and noise from ADC.
	randomSeed(
					getAdjustedMicroSecTime() 
					^ static_cast<uint64_t>(analogReadTemp())
					^ get_rand_64()
			);

	Serial.printf("Pico %d ready!", PICO_ID);
	Serial.println();
}

/**
 * Core 1 init: steppers
 */
void setup1() {
	initializeStepperMotors(steppers);
}

/**
 * Gets data from sensors and manages communication with SBC
 */
void loop() {
	// Program is still alive, reset watchdog
	watchdog_update();

	// Get sensor data
	DataFrame dataFrame = getDataFrame(&imu);
	pushIfPresent<DataFrame>(dataFrame, dataFrames);

	BMP388Data data0 = readBMP388Data(bmpSensors[0]);
	pushIfPresent<BMP388Data>(data0, bmpData0);

	BMP388Data data1 = readBMP388Data(bmpSensors[1]);
	pushIfPresent<BMP388Data>(data1, bmpData1);

	// Ask for and execute commands
	receiveCommandsFromComputer();

	// Prevent tight loops
	delay(100);
}

/**
 * Exclusively drives the steppers, we need to do this in separate
 * process to prevent stepper stuttering.
 */
void loop1() {
	watchdog_update();
	steppers[0].moveStep(); // Poll stepper1
	steppers[1].moveStep(); // Poll stepper2
}
