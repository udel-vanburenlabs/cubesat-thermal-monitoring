/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "HallEncoder.h"

void HallEncoder::encoderISR(HallEncoder* encoder) {
  noInterrupts();

  encoder->encoderLastTime = encoder->encoderTime;
  encoder->encoderTime = getAdjustedMicroSecTime();

  interrupts();
}

void HallEncoder::encoderISR(void* param) {
    encoderISR(static_cast<HallEncoder*>(param));
}

unsigned long HallEncoder::getCurrentEncoderTime() {
    return encoderTime;
}

unsigned long HallEncoder::getLastEncoderTime() {
    return encoderLastTime;
}

double HallEncoder::read() {
    delay(ENC_LOOP_DELAY);
    encoderPeriods[periodsI] = (encoderTime - encoderLastTime) / 1000;
    periodsI += 1;
    enc10sAvgI += 1;

    if (periodsI >= ENCODER0_PERIODS_MAXLEN) {
        periodsI = 0;
    }

    if (enc10sAvgI >= ENC_10S_AVG_LEN) {
        enc10sAvgI = 0;
    }

    unsigned long sum = 0;
    for (int i = 0; i < ENCODER0_PERIODS_MAXLEN; i++) {
        sum += encoderPeriods[i];
    }

    double rpm0 = (60.0 * 1000.0 / (sum / (double) ENCODER0_PERIODS_MAXLEN)) / ENC_PULSES_PER_REV;
    encoder10sAvgArr[enc10sAvgI] = rpm0;

    double enc10sSum = 0;
    for (int j = 0; j < ENC_10S_AVG_LEN; j++) {
        enc10sSum += encoder10sAvgArr[j];
    }

    double encoder10sAvg = enc10sSum / (double) ENC_10S_AVG_LEN;

    encoderTime = getAdjustedMicroSecTime();

    return encoder10sAvg;
}

HallEncoder::HallEncoder(pin_size_t pin) : pin(pin) {
    for (int i = 0; i < ENCODER0_PERIODS_MAXLEN; i++) {
        encoderPeriods[i] = 0;
    }

    for (int i = 0; i < ENC_10S_AVG_LEN; i++) {
        encoder10sAvgArr[i] = 0.0;
    }

    pinMode(pin, INPUT_PULLUP);
    attachInterruptParam(digitalPinToInterrupt(pin), &encoderISR, FALLING, this);
}

HallEncoder::~HallEncoder() {
    detachInterrupt(digitalPinToInterrupt(this->pin));
}
