/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#ifndef _CUBELAB_COMMANDS_H
#define _CUBELAB_COMMANDS_H

#include <ArduinoJson.h>

#include <list>

#include "util.h"

extern std::list<DataFrame> dataFrames;

#ifndef _UNIT_TEST
  extern MAX518 max518;

  extern Stepper steppers[];
#endif

extern std::list<BMP388Data> bmpData0;
extern std::list<BMP388Data> bmpData1;

// HttpRequest encodeResultPacket(ResultCode code);
std::string encodeDataFrame();

// Function prototypes for each command
void handleStartStepper1(uint32_t stepperSpeed);
void handleStartStepper2(uint32_t stepperSpeed);
void handleStopStepper1();
void handleStopStepper2();
void handleResetMCU();
void handleClearDataBuffer();
void handleSetLaserState(unsigned int flowFacility, uint32_t brightness);

bool didHostACK();

void getIncomingCommand();
void executeCommand(std::string& command);

#endif // _CUBELAB_COMMANDS_H
