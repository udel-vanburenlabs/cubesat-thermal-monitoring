/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

// Lazy copy from previously generated NanoPB objects. Will probably be replaced by common data models.
// ... at some point

// Include hardware drivers if we're on actual hardware
#ifndef _UNIT_TEST
  #include "StepperAsync5.h"
  #include "MAX51X.h"
#endif

// Use StepperAsync5 if we're on actual hardware
#ifndef _UNIT_TEST
  typedef StepperAsync5 Stepper;
#endif

/* Struct definitions */
typedef struct _pico_BMP388Data {
    uint64_t timestamp;
    double temperature;
    double pressure;
} pico_BMP388Data;

typedef struct _pico_SensorData {
    uint64_t timestamp;
    float picoTemperature;
    float accel_x;
    float accel_y;
    float accel_z;
    float gyro_x;
    float gyro_y;
    float gyro_z;
} pico_SensorData;

typedef struct _pico_UARTDataPacket {
    uint32_t picoId;
    bool has_bmpData0;
    pico_BMP388Data bmpData0;
    bool has_bmpData1;
    pico_BMP388Data bmpData1;
    bool has_sensorData;
    pico_SensorData sensorData;
    uint64_t timestamp;
} pico_UARTDataPacket;

typedef struct PicoDataframeDto {
    unsigned long frameId;
    int picoId;
    unsigned long timestamp;
    float picoTemp;
    float sensTemp0;
    float sensTemp1;
    float sensPress0;
    float sensPress1;
    float sensHum0;
    float sensHum1;
    float sensAccelX0;
    float sensAccelX1;
    float sensAccelY0;
    float sensAccelY1;
    float sensAccelZ0;
    float sensAccelZ1;
    float sensGyroX0;
    float sensGyroX1;
    float sensGyroY0;
    float sensGyroY1;
    float sensGyroZ0;
    float sensGyroZ1;
    float rpm0;
    float rpm1;

    unsigned int messageId;
} PicoDataframeDto;

typedef _pico_BMP388Data BMP388Data;
typedef _pico_SensorData DataFrame;
typedef _pico_UARTDataPacket UARTDataPacket; // TODO clean

enum CaseStatus {
  QUEUED = 1,
  IN_PROGRESS = 2,
  PAUSED = 3,
  FAILED = 4,
  COMPLETE = 5
};

enum LogLevel {
  DEBUG = 1,
  VERBOSE = 2,
  INFO = 3,
  WARNING = 4,
  ERROR = 5,
  CRITICAL = 6
};

enum CaptureType {
  CONT = 1,
  BURST = 2
};

enum CaseType {
  STEADY_STATE = 1,
  DECAY = 2
};

typedef struct CaseParams {
  uint32_t caseId;
  uint32_t flowFacility;
  uint32_t captureDurationSec;
  uint32_t stepperSpeed;
  CaseType caseTypeId;
  uint32_t spinDurationSec;
  uint32_t laserBrightness;
  uint32_t minSecSinceLast;
  float maxTemp;
  float minTemp;
  CaptureType captureTypeId;
  uint32_t burstPeriodMs;
  uint32_t burstSize;
  uint32_t targetFps;
} CaseParams;

typedef struct CubelabCase {
  uint32_t caseId;
  CaseStatus caseStatusId;
  uint64_t startTimestamp;
  uint64_t completeTimestamp;
} CubelabCase;

typedef struct CaseLogMessage {
  uint32_t caseId;
  uint32_t eventId;
  uint64_t timestamp;
  std::string logMessage;
  LogLevel logLevel;
} CaseLogMessage;
