/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include <string>
#include <sstream>
#include <map>

#include <iostream>

#include "../ILogger.h"

ConsoleLogger* logger;

class ConsoleLogger : public ILogger {
    public:
        virtual void debugln(const char* msg) {
            std::cout << msg << std::endl;
        }

        virtual void debug(const char* msg) {
            std::cout << msg;
        }

        virtual void debugf(const char* format, ...) {
            va_list args;
            
            printf(format, args);
        }
};

typedef struct CommandStruct {
  std::string command;
  int arg1 = 0;
  int arg2 = 0;
  int arg3 = 0;
} CommandStruct;

CommandStruct parseCommand(const std::string& commandStr) {
        CommandStruct cmd;
    std::istringstream iss(commandStr);

    // Read the command first
    iss >> cmd.command;

    // Map command to the number of expected integer arguments
    std::map<std::string, int> commandArgs = {
        {"SET_LASER", 2},
        {"SET_STEPPER", 2},
        {"FLUSH_DATA", 0},
        {"START_CASE", 1},
        {"WAIT", 1},
        {"END_CASE", 1}
    };

    int expectedArgs = commandArgs[cmd.command];  // Fetch the expected number of arguments for this command

    // Read the arguments based on expected count
    if (expectedArgs > 0 && !(iss >> cmd.arg1)) {
        iss.clear();  // Clear the error state for further operations
    }
    if (expectedArgs > 1 && !(iss >> cmd.arg2)) {
        iss.clear();
    }
    if (expectedArgs > 2 && !(iss >> cmd.arg3)) {
        iss.clear();
    }

    // Check if the extraction failed only after attempting to read the expected number of arguments
    if (iss.fail()) {  // Also check if we're not just at the end of the string
        logger->debugln("[ERROR] Failed to parse command string " + commandStr);
    }

    return cmd;
}

int main() {
    CommandStruct testStruct = parseCommand("SET_STEPPER 0 400");

    std::cout << testStruct.command << std::endl;
    std::cout << testStruct.arg1 << std::endl;
    std::cout << testStruct.arg2 << std::endl;
    std::cout << testStruct.arg3 << std::endl;

}