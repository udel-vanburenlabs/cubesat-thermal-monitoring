#include <vector>
#include <string>
#include <sstream>

#include <iostream>

struct HttpResponse {
    std::string body;
};

std::vector<std::string> parseCommandsFromResp(const HttpResponse& resp) {
    std::vector<std::string> commands;
    std::string body = resp.body;
    
    // Remove the square brackets from the start and end of the JSON array
    size_t start = body.find('[');
    size_t end = body.rfind(']');
    
    if (start == std::string::npos || end == std::string::npos) {
        // TODO log invalid JSON message
        return commands;  // Return empty vector if JSON format is incorrect
    }

    // Extract the content inside the square brackets
    std::string content = body.substr(start + 1, end - start - 1);
    std::istringstream iss(content);
    std::string token;

    // Process each JSON string element
    while (std::getline(iss, token, ',')) {
        size_t start_quote = token.find('"');
        size_t end_quote = token.rfind('"');

        if (start_quote != std::string::npos && end_quote != std::string::npos && start_quote != end_quote) {
            std::string command = token.substr(start_quote + 1, end_quote - start_quote - 1);
            commands.push_back(command);
        }
    }

    return commands;
}

int main() {
        HttpResponse resp = {R"([
        "SET_LASER 0 0",
        "SET_LASER 1 0",
        "SET_STEPPER 0 0",
        "SET_STEPPER 1 0",
        "FLUSH_DATA",
        "START_CASE 1",
        "SET_LASER 0 255",
        "SET_STEPPER 0 400",
        "WAIT 5",
        "SET_STEPPER 0 0",
        "WAIT 5",
        "END_CASE 1"
    ])"};

    std::vector<std::string> commands = parseCommandsFromResp(resp);
    for (const std::string& cmd : commands) {
        std::cout << cmd << std::endl;
    }

    return 0;
}