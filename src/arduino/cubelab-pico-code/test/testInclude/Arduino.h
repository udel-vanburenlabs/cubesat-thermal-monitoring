#include <chrono>
#include <cstdarg>
#include <cstdint>
#include <cstdio>
#include <string>

#include "Wire.h"

extern TwoWire Wire;

/**
 * Test replacement for Pico time_us_64() function
 */
static inline uint64_t time_us_64() {
    auto now = std::chrono::system_clock::now();
    auto microsec = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
    return static_cast<uint64_t>(microsec.count());
}

/**
 * Test replacement for Arduino Serial functions
 */
class DummySerial {
public:
    inline void print(std::string str);
    inline void println(std::string str);
    inline void printf_P(std::string str, ...);
    inline void write(char c);
};

inline void DummySerial::print(std::string str) {
    puts(str.c_str());
}

inline void DummySerial::println(std::string str) {
    printf("%s\n", str.c_str());
}

inline void DummySerial::printf_P(std::string str, ...) {
    va_list args;
    printf(str.c_str(), args);
}

inline void DummySerial::write(char c) {
    putchar(c);
}


#define Serial DummySerial()
