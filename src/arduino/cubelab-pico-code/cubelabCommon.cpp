/*
 MIT License

 Copyright (c) 2023 Galen Nare

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

// NOTE no unit tests due to reliance on Pico hardware
#include "cubelabCommon.h"

using std::string;

// TODO remove
#define _ALLOW_IMU_FAIL 1
#define _ALLOW_BMP388_FAIL 1
#define _ALLOW_I2C_FAIL 1

extern int currentCaseId;

extern std::list<DataFrame> dataFrames;
extern std::list<BMP388Data> bmpData0;
extern std::list<BMP388Data> bmpData1;

void initializeI2C() {
	pinMode(LED, OUTPUT);

	Wire.setSCL(I2C0_SCL);
	Wire.setSDA(I2C0_SDA);
	Wire.begin();

	Wire1.setSCL(I2C1_SCL);
	Wire1.setSDA(I2C1_SDA);
	Wire1.begin();

	if (!Wire.available() || !Wire1.available()) {
		// Triple blink once per sec (Blink error 3)

#ifndef _ALLOW_I2C_FAIL
    while (1) {
      blinkError(3);
    }
#endif
	}
}

void initializeIMU(Adafruit_ADXL375 *imu) {
	imu->setDataRate(ADXL3XX_DATARATE_12_5_HZ);
	bool imuReady = imu->begin();

	if (!imuReady) {
		Serial.println(
				"[ERROR] Failed to initialize ADXL375 sensor! Check connection!");

#ifndef _ALLOW_IMU_FAIL
    // Double blink once per sec (Blink error 2)
    while (1) {
      blinkError(2);
    }
#endif
	}

	Serial.println("[DEBUG] ADXL375 ready.");
}

void initializeLasers(MAX518 *max518) {
	max518->begin(Wire1);
	max518->powerOffBoth();
}

void initializeBMP388Sensors(Adafruit_BMP3XX *bmpSensors) {
	bool sensorsReady = bmpSensors[0].begin_I2C(BMP388_ADDR0, &Wire)
			&& bmpSensors[1].begin_I2C(BMP388_ADDR1, &Wire);

	if (!sensorsReady) {
		Serial.println(
				"[ERROR] Failed to initialize BMP388 sensors! Check connection!");

		// Single blink once per sec (Blink error 1)
#ifndef _ALLOW_BMP388_FAIL
    while (1) {
      blinkError(1);
    }
#endif
	}

	bmpSensors[0].setTemperatureOversampling(BMP3_OVERSAMPLING_8X);
	bmpSensors[1].setTemperatureOversampling(BMP3_OVERSAMPLING_8X);

	bmpSensors[0].setPressureOversampling(BMP3_OVERSAMPLING_4X);
	bmpSensors[1].setPressureOversampling(BMP3_OVERSAMPLING_4X);

	bmpSensors[0].setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);
	bmpSensors[1].setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);

	bmpSensors[0].setOutputDataRate(BMP3_ODR_12_5_HZ);
	bmpSensors[1].setOutputDataRate(BMP3_ODR_12_5_HZ);

	Serial.println("[DEBUG] BMP388 Sensors initialized.");
}

void initializeStepperMotors(Stepper motors[]) {
	pinMode(STEPPERS_EN, OUTPUT);
	digitalWrite(STEPPERS_EN, LOW);

	motors[0].setSpeed(0);
	motors[1].setSpeed(0);
}

DataFrame getDataFrame(Adafruit_ADXL375 *imu) {
	DataFrame framePtr;
	framePtr.picoTemperature = analogReadTemp();

	sensors_event_t event;
	imu->getEvent(&event);

	framePtr.accel_x = event.acceleration.x;
	framePtr.accel_y = event.acceleration.y;
	framePtr.accel_z = event.acceleration.z;
	framePtr.gyro_x = event.gyro.x;
	framePtr.gyro_y = event.gyro.y;
	framePtr.gyro_z = event.gyro.z;

	framePtr.timestamp = getAdjustedMicroSecTime();

	return framePtr;
}

BMP388Data readBMP388Data(Adafruit_BMP3XX sensor) {
	BMP388Data data;
	if (sensor.performReading()) {
		data.temperature = sensor.temperature;
		data.pressure = sensor.pressure;
		data.timestamp = getAdjustedMicroSecTime();
	}

	return data;
}

void executeCommand(CommandStruct &cs) {
	if (cs.command == "FLUSH_DATA") {
		// TODO write frames to API -- the Pico Tunnel does not have the #DATAFRAME or #END_DATA_FRAMES commands implemented yet.
		while (dataFrames.size() > 0 && bmpData0.size() > 0 && bmpData1.size() > 0) {
			std::string jsonFrame = encodeDataFrame();
			dataFrames.pop_front();
			bmpData0.pop_front();
			bmpData1.pop_front();

			Serial.print("#DATAFRAME ");
			Serial.println(jsonFrame.c_str());
		}
		Serial.println("#END_DATA_FRAMES");
	} else if (cs.command == "START_CASE") {
		currentCaseId = cs.arg1;
		Serial.printf("[INFO] Starting case %d\n", currentCaseId);
		Serial.println();
	} else if (cs.command == "END_CASE") {
		Serial.printf("[INFO] Ending case %d\n", currentCaseId);
		Serial.println();
		currentCaseId = -1;
	} else if (cs.command == "SET_LASER") {
		handleSetLaserState(cs.arg1, cs.arg2);
	} else if (cs.command == "SET_STEPPER") {
		if (cs.arg1 == 0) {
			handleStartStepper1(cs.arg2);
		} else if (cs.arg1 == 1) {
			handleStartStepper2(cs.arg2);
		}
	} else if (cs.command == "WAIT") {
		delay(cs.arg1 * 1000L);
	} else {
		Serial.printf("[ERROR] Could not execute commandstruct %s %d %d %d",
				cs.command.c_str(), cs.arg1, cs.arg2, cs.arg3);
		Serial.println();
	}
}

void executeCommands(std::vector<string> commands) {
	for (string &commandWithParams : commands) {
		CommandStruct cstruct = parseCommand(commandWithParams);

		executeCommand(cstruct);
	}
}

void receiveCommandsFromComputer() {
	if(Serial.available()) {
		getIncomingCommand();
	}

	Serial.println("#GET_COMMANDS");
}

void blinkError(int blinksPerSec) {
	for (int i = 0; i < blinksPerSec; i++) {
		digitalWrite(LED, HIGH);
		delay(50);
		digitalWrite(LED, LOW);
		delay(50);
	}

	delay(1000 - (blinksPerSec * 10));
}
