/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#include "util.h"

extern uint64_t systemMicroSecOffset;

uint32_t picoUuid() {
  pico_unique_board_id_t buffer;
  pico_get_unique_board_id(&buffer);

  uint64_t result = 0;
  for (int i = 0; i < 8; ++i) {
      result |= buffer.id[i] << (8 * i);
  }
  return static_cast<uint32_t>(result);
}

// Function to trim leading and trailing whitespace
std::string trimWhitespace(const std::string& input) {
    std::string result = input;
    result.erase(result.begin(), std::find_if(result.begin(), result.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
    result.erase(std::find_if(result.rbegin(), result.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), result.end());
    return result;
}

// Function to trim the leading "/api/dataframe/message/" and any whitespace
std::string trimDataframeMessagePathAndWhitespace(const std::string& input) {
    const std::string prefix = "/api/dataframe/message/";
    std::string result = input;

    // Remove the leading "/api/dataframe/message/" if it exists
    if (result.find(prefix) == 0) {
        result = result.substr(prefix.length());
    }

    // Trim leading and trailing whitespace
    return trimWhitespace(result);
}

std::string toUpperCase(const std::string& input) {
    std::string result = input;
    std::transform(result.begin(), result.end(), result.begin(),
                   [](unsigned char c){ return std::toupper(c); });
    return result;
}

uint64_t getAdjustedMicroSecTime() {
	return time_us_64() + systemMicroSecOffset;
}

void blink(int times, int periodMs) {
	for (int i = 0; i < times; i++) {
		digitalWrite(LED, HIGH);
		delay(periodMs);
		digitalWrite(LED, LOW);
		delay(periodMs);
	}
}
