/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#ifndef _PICO_UTIL_H
#define _PICO_UTIL_H

#include <cstdint>
#include <vector>
#include <list>
#include <sstream>
#include <string>
#include <cstring>
#include <algorithm>
#include <pico/unique_id.h>

#include "picoTypes.h"
#include "picoConstants.h"

// Various functions for common MCU operations

/**
 * Gets the UUID of the MCU, should be unique to the hardware
*/
uint32_t picoUuid();

/**
 * Gets the current time in microseconds, including the time offset from
 * the SBC.
*/
uint64_t getAdjustedMicroSecTime();

/**
 * Trims the whitespace from the beginning and end of a string.
*/
std::string trimWhitespace(const std::string& str);

/**
 * Trips the API path and whitespace from a dataframe message.
*/
std::string trimDataframeMessagePathAndWhitespace(const std::string& input);

/**
 * Converts a string to uppercase.
*/
std::string toUpperCase(const std::string& input);

/**
 * Blinks the onboard LED a number of times with the specified period.
*/
void blink(int times, int periodMs);

/**
 * Checks if an object is equal to a default initialized instance of its type.
*/
template <typename T>
bool isDefaultInitialized(const T& obj) {
    T defaultObj{};
    return std::memcmp(&defaultObj, &obj, sizeof(T)) == 0;
}

/**
 * Pushes an object to a list if it is not default initialized.
*/
template <typename T>
bool pushIfPresent(const T& obj, std::list<T> vec) {
	if (!isDefaultInitialized(obj)) {
		vec.push_back(obj);
		return true;
	}

	return false;
}

#endif // _PICO_UTIL_H
