/*
  MIT License

  Copyright (c) 2023 Galen Nare

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

#ifndef __COMMAND_PARSER_H
#define __COMMAND_PARSER_H

#include <vector>
#include <map>
#include <string>
#include <sstream>

#include "util.h"

typedef struct CommandStruct {
  std::string command = "";
  unsigned int arg1 = 0;
  unsigned int arg2 = 0;
  unsigned int arg3 = 0;
} CommandStruct;

// Function prototypes for handling communication with Raspberry Pi Pico

/**
 * Parses a single command string.
 * Any extra arguments are ignored.
 * Only gives an error message if the command is not recognized or a required
 * argument is missing.
*/
std::vector<std::string> parseCommandsFromResp(const std::string& body);

/**
 * Parses the commands from the JSON array in the UART payload.
 * Returns a vector of command strings.
 * Expects a JSON array, not a full object.
*/
CommandStruct parseCommand(const std::string& commandStr);

#endif // __COMMAND_PARSER_H
