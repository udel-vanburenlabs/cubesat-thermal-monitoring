#include "MAX51X.h"
#include "Stepper.h"
#include "Wire.h"

#define STEPS 200 // 200 steps/rev (1.8deg per step)

#define ENCODER0 26
#define ENCODER0_PERIODS_MAXLEN 50
#define ENC_PULSES_PER_REV 4.0
#define ENC_LOOP_DELAY 10
#define ENC_10S_AVG_LEN (10000 / ENC_LOOP_DELAY)

void encoderISR();

volatile unsigned long encoderTime = 0;
unsigned long encoderLastTime = 0;

unsigned long encoderPeriods[ENCODER0_PERIODS_MAXLEN];
unsigned int periodsI = 0;

double encoder10sAvgArr[ENC_10S_AVG_LEN];
unsigned int enc10sAvgI = 0;

Stepper stepper1(STEPS, 15, 14, 16, 17);
Stepper stepper2(STEPS, 6, 7, 8, 9);

int c = 150;
int d = 0;
int maxs = 300;

MAX518 max518(0x2c);

void setup() {
  pinMode(20, OUTPUT);
  digitalWrite(20, HIGH);

  for (int i = 0; i < ENCODER0_PERIODS_MAXLEN; i++) {
    encoderPeriods[i] = 0;
  }

  for (int i = 0; i < ENC_10S_AVG_LEN; i++) {
    encoder10sAvgArr[i] = 0.0;
  } 
}

void setup1() {
  pinMode(25, OUTPUT);

  pinMode(ENCODER0, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(ENCODER0), encoderISR, FALLING);

  Wire.setSCL(13);
  Wire.setSDA(12);
  Wire.begin();

  Wire1.setSCL(11);
  Wire1.setSDA(10);
  Wire1.begin();

  max518.begin(Wire1);
  max518.setBothDac(255, 255);

  Serial.begin(115200);
}

void loop() {
  // if (d == 10000) {
  //   d = 0;
  //   c = 300;
  // }

  // d++;

  if (c < maxs) {
    c += 2;
    stepper1.setSpeed(c);
    stepper2.setSpeed(c);
    // Serial.println(c);
  }

  stepper1.step(-1);
  stepper2.step(1);
}

void loop1() {
  delay(ENC_LOOP_DELAY);
  encoderPeriods[periodsI] = (encoderTime - encoderLastTime) / 1000;
  periodsI += 1;
  enc10sAvgI += 1;

  if (periodsI >= ENCODER0_PERIODS_MAXLEN) {
    periodsI = 0;
  }

  if (enc10sAvgI >= ENC_10S_AVG_LEN) {
    enc10sAvgI = 0;
  }

  unsigned long sum = 0;
  for (int i = 0; i < ENCODER0_PERIODS_MAXLEN; i++) {
    sum += encoderPeriods[i];
  }

  double rpm0 = (60.0 * 1000.0 / (sum / (double) ENCODER0_PERIODS_MAXLEN)) / ENC_PULSES_PER_REV;
  encoder10sAvgArr[enc10sAvgI] = rpm0;

  double enc10sSum = 0;
  for (int j = 0; j < ENC_10S_AVG_LEN; j++) {
    enc10sSum += encoder10sAvgArr[j];
  }

  double encoder10sAvg = enc10sSum / (double) ENC_10S_AVG_LEN;

  Serial.print(digitalRead(ENCODER0) * 100);
  Serial.print(" ");
  Serial.print(300);
  Serial.print(" ");
  // Serial.print(enc10sSum);
  // Serial.print(" ");
  Serial.print(encoder10sAvg);
  Serial.print(" ");
  Serial.println(rpm0);

  encoderTime = time_us_64();
  
}

void encoderISR() {
  noInterrupts();

  encoderLastTime = encoderTime;
  encoderTime = time_us_64(); 

  interrupts();
}